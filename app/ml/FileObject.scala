import java.io.{File, PrintWriter}

import scala.collection.mutable
import scala.io.Source

/**
 * Created by ph on 5/11/15.
 */
object FileObject {
  val bookWriter = new PrintWriter(new File("/home/ph/Desktop/book-ratings/BX-Books1.csv"))
  var lineNo = 1
  val ratings = new mutable.HashMap[String, Int]()
  for (line <- Source.fromFile("/home/ph/Desktop/book-ratings/BX-Books.csv", "ISO-8859-1")
    .getLines()) {
    ratings += (line.split(";")(0) -> lineNo)
    bookWriter.write(lineNo + ";" + line + "\n")
    lineNo += 1
  }
  bookWriter.close()

  val ratingWriter = new PrintWriter(new File("/home/ph/Desktop/book-ratings/BX-Book-Ratings1.csv"))
  for (line <- Source.fromFile("/home/ph/Desktop/book-ratings/BX-Book-Ratings.csv", "ISO-8859-1")
    .getLines()) {
    val l = line.split(";")
    val userId = l(0)
    val ISBN = l(1)
    val rating = l(2)
    if (rating != "0") {
      ratings.get("\"" + ISBN + "\"").foreach {
        i => ratingWriter.write(userId + ";" + i + ";" + rating + "\n")
      }
    }

  }
  ratingWriter.close()
}

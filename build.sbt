name := """playml"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  "org.apache.spark" %% "spark-mllib" % "1.3.1",
  "org.apache.spark" %% "spark-streaming" % "1.3.1",
  "mysql" % "mysql-connector-java" % "5.1.35"
)

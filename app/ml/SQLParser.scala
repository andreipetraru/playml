import java.io.{File, PrintWriter}

import scala.collection.immutable.HashSet
import scala.collection.mutable
import scala.io.Source

/**
 * Utilities for parsing the dataset into SQL insert statements
 */
object SQLParser {
  def firstLast(s: String): String = {
    if (s.length > 2)
      s.substring(1, s.length - 1)
    else
      s
  }

  def capitalize(s: String): String = {
    if (s.length < 2)
      s
    else
      firstLast(s).toLowerCase.split(' ').map(_.capitalize).mkString(" ")
  }

  def quote(s: String): String = {
    "'" + s + "'"
  }

  def booksAndAuthors = {
    val authNames = new mutable.HashSet[String]
    val authMap = new mutable.HashMap[String, Int]
    var id = 1
    val authorWriter = new PrintWriter(new File("/home/ph/Desktop/sql/authors.sql"))
    val bookWriter = new PrintWriter(new File("/home/ph/Desktop/sql/books.sql"))
    for (line <- Source.fromFile("/home/ph/Desktop/book-ratings/BX-Books1.csv", "ISO-8859-1").getLines()) {
      val l = line.split(";")
      val author = quote(capitalize(l(3)).replace("'", "\\'"))
      authNames += author
    }

    authNames.foreach {
      name =>
        val query = "INSERT into AUTHOR (id, name, about, gender, website, image_url, birth_date) VALUES (" +
          id + ", " +
          name + ", " +
          "'Detailed Info about author', 'MALE', 'http://www.google.com/', 'http://shakespeare.mit.edu/shake.gif', '1900-01-01');\n"
        authorWriter.write(query)
        authMap.put(name, id)
        id += 1
    }

    for (line <- Source.fromFile("/home/ph/Desktop/book-ratings/BX-Books1.csv", "ISO-8859-1").getLines()) {
      val l = line.split(";")
      val id = l(0)
      val isbn = quote(firstLast(l(1)))
      val title = quote(capitalize(l(2)).replace("'", "\\'"))
      val author = quote(capitalize(l(3)).replace("'", "\\'"))
      val authorId = authMap.get(author).get
      val year = quote(firstLast(l(4)))
      val publisher = quote(firstLast(l(5)).replace("'", "\\'"))
      val imgS = quote(firstLast(l(6)))
      val imgM = quote(firstLast(l(7)))
      val imgL = quote(firstLast(l(8)))
      val q = List(id, isbn, title, authorId, year, publisher, imgS, imgM, imgL).mkString(", ")
      val query = "INSERT into BOOK (id, isbn, title, author_id, year, publisher, img_small, img_medium, img_large, description, edition_language, pages) VALUES (" +
        q +
        ", 'Book description','ENGLISH', 250);\n"
      bookWriter.write(query)
    }

    authorWriter.close()
    bookWriter.close()
  }

  def users = {
    val writer = new PrintWriter(new File("/home/ph/Desktop/sql/users.sql"))
    for (line <- Source.fromFile("/home/ph/Desktop/book-ratings/BX-Users.csv", "ISO-8859-1").getLines()) {
      val l = line.split(";")
      val id = firstLast(l(0))
      val location = firstLast(l(1))
      val q = id + ", " + "'" + location + "'" + ", 25, "
      val query = "INSERT INTO CUSTOMER(id, location, age, email_address, firstname, lastname, password, username) VALUES ("
      val rest = "'user" + id + "@gmail.com', null, null, '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 'user" + id + "');\n"
      writer.write(query + q + rest)
    }
    writer.close()
  }

  def reviews = {
    val writer = new PrintWriter(new File("/home/ph/Desktop/data/ratings.sql"))
    var id = 1
    for (line <- Source.fromFile("/home/ph/Desktop/data/BX-Book-Ratings-book-id.csv", "ISO-8859-1")
      .getLines()) {
      val l = line.split(";")
      val userId = l(0)
      val bookId = l(1)
      val rating = l(2)
      if (rating != "0") {
        val q = List(id, userId, bookId, rating).mkString(", ")
        var query = "INSERT INTO REVIEW(id, customer_id, book_id, rating, publish_date, comment) VALUES("
        query += q
        query += ", '2007-01-01'"
        query += ", 'Great book!');\n"
        writer.write(query)
        query = ""
        id += 1
      }
    }
    writer.close()
  }

  def roles = {
    val writer = new PrintWriter(new File("/home/ph/Desktop/sql/user_roles.sql"))
    for (id <- 1 to 278858) {
      writer.write("INSERT INTO CUSTOMER_ROLE (customer_id, role_id) VALUES(" + id + ", 2);\n")
    }
    writer.close()
  }
}

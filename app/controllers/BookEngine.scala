package controllers

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.mllib.recommendation.ALS
import org.apache.spark.mllib.recommendation.Rating
import org.jblas.DoubleMatrix

/**
 * @author ph
 */
object BookEngine {

  var model = {
    val logFile = "/sparkLog"
    val conf = new SparkConf().setAppName("GoodBooks").setMaster("local[2]")
    val sc = new SparkContext(conf)
    val rawData = sc.textFile("/home/ph/Desktop/data/ratings.csv")
    var rawRatings = rawData.map(_.split(";").take(3))
    val ratings = rawRatings.map {
      case Array(user, movie, rating) =>
        Rating(user.toInt, movie.toInt, rating.toDouble)
    }

    ALS.train(ratings, 50, 10, 0.01)
  }

  def recommendByUser(userId: Int, top: Int) = {
    val topKRecs = model.recommendProducts(userId, top)
    topKRecs
  }

  def recommendByBook(bookId: Int, top: Int) = {
    val aMatrix = new DoubleMatrix(Array(1.0, 2.0, 3.0))

    def cosineSimilarity(vec1: DoubleMatrix, vec2: DoubleMatrix): Double = {
      vec1.dot(vec2) / (vec1.norm2() * vec2.norm2())
    }

    val itemFactor = model.productFeatures.lookup(bookId).head
    val itemVector = new DoubleMatrix(itemFactor)
    println(cosineSimilarity(itemVector, itemVector))

    val sims = model.productFeatures.map {
      case (id, factor) =>
        val factorVector = new DoubleMatrix(factor)
        val sim = cosineSimilarity(factorVector, itemVector)
        (id, sim)
    }

    val sortedSims = sims.top(top + 1)(Ordering.by[(Int, Double), Double] {
      case (id, similarity) => similarity
    })
    sortedSims
  }
}
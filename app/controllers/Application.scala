package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._

object Application extends Controller {

  def user(id: Int) = Action {
    val ratings = BookEngine.recommendByUser(id, 10)
    val ids = ratings.map { rating => rating.product }
    Ok(Json.toJson(ids))
  }

  def book(id: Int) = Action {
    val books = BookEngine.recommendByBook(id, 15)
    val ids = books.map {
      case (bookId, bookRating) => bookId
    }
    Ok(Json.toJson(ids.filter { bookId => id != bookId }))
  }

}
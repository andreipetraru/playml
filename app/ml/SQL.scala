package ml

import java.sql.DriverManager
import java.sql.ResultSet
import java.io.PrintWriter
import java.io.File

/**
 * @author ph
 */
object SQL extends App{
  val conn_str = "jdbc:mysql://localhost:3306/goodbooks?user=root&password=root"

  val conn = DriverManager.getConnection(conn_str)
  try {
    val statement = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)

    val rs = statement.executeQuery("SELECT customer_id, book_id, rating FROM REVIEW")

    val bookWriter = new PrintWriter(new File("/home/ph/Desktop/data/ratings.csv"))

    while (rs.next) {
      val customer = rs.getString("customer_id")
      val book = rs.getString("book_id")
      val rating = rs.getString("rating")
      val line = List(customer, book, rating).mkString(";") + "\n"
      bookWriter.write(line)
    }
    bookWriter.close()
  } finally {
    conn.close()
  }
}